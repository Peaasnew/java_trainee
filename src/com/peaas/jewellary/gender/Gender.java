package com.peaas.jewellary.gender;

import com.peaas.jewellary.material.MaterialType;

import java.util.Comparator;

import com.peaas.jewellary.material.MaterialColor;
import com.peaas.jewellary.ornament.*;

public class Gender extends Ornament implements Comparable<Gender>, Comparator<Gender> {

	// private ArrayList<Gender> maleOrnament = new ArrayList<Gender>();
	// private ArrayList<Gender> femaleOrnament = new ArrayList<Gender>();
	protected Gender newOrnaments;
	protected GenderEnum gender;
	protected int weight;
	private static int hash = 19;

	public Gender(GenderEnum gender, OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor,
			int weight) {
		super(ornamentType, materialType, materialColor);
		this.gender = gender;
		this.weight = weight;

	}

	public boolean equals(Object object) {
		boolean result = false;
		if (object == null) {
			result = false;
		} else {
			Gender gender = (Gender) object;
			if (gender.ornamentType == this.ornamentType && gender.materialType == this.materialType) {
				result = true;
			}
		}

		return result;
	}

	public int hashCode() {
		hash = hash + this.ornamentType.hashCode();
		hash = hash + this.materialType.hashCode();
		hash = hash + this.weight;
		hash++;
		return hash;
	}

	public int compareTo(Gender gender) {
		if (weight == gender.weight)
			return 0;
		else if (weight >= gender.weight)
			return 1;
		else
			return -1;
	}

	public int compare(Gender g1, Gender g2) {
		return g1.ornamentType.compareTo(g2.ornamentType);
	}

	public void add() {
		/*
		 * newOrnaments = new Gender(gender, ornamentType, materialType,
		 * materialColor, weight); if (gender == GenderEnum.MALE) {
		 * maleOrnament.add(newOrnaments); } else {
		 * femaleOrnament.add(newOrnaments); }
		 */
	}

	public void display() {
		/*
		 * Iterator<Gender> show ; if(gender == GenderEnum.MALE) { show =
		 * maleOrnament.iterator(); } else { show = femaleOrnament.iterator(); }
		 * while(show.hasNext()) { Gender gender = (Gender)show.next();
		 * System.out.println("gender  Type    Material   Color  weight");
		 * System.out.println(gender.gender+" "+gender.ornamentType+" "
		 * +gender.materialType+" "+gender.materialColor+" "+gender.weight); } }
		 * 
		 * public int totalCost() { this.cost = cost*this.weight; return cost; }
		 */
	}
}
