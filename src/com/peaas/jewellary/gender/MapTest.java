package com.peaas.jewellary.gender;

import java.util.*;

import com.peaas.jewellary.material.MaterialColor;
import com.peaas.jewellary.material.MaterialType;
import com.peaas.jewellary.ornament.OrnamentType;

public class MapTest extends HashMap<Integer, MapTest> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<Integer, MapTest> maleMap = new HashMap<Integer, MapTest>();
	private static int key;

	int weight;
	OrnamentType ornamentType;
	MaterialType materialType;
	MaterialColor materialColor;

	public MapTest(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor, int weight) {

	}

	/*
	 * public boolean equals(Object object) { boolean result = false ;
	 * Map<Integer, Gender> map; map = (Map<Integer, Gender>)object ; if(object
	 * == null || map. )
	 * 
	 * return result; }
	 */

	public void add() {
		MapTest newOrnaments = new MapTest(ornamentType, materialType, materialColor, weight);

		if (key != -1) {
			maleMap.put(key, newOrnaments);
		}
		key++;
	}

	public boolean equals(Object object) {
		boolean result = false;
		if (object == null) {
			result = false;
		} else {
			MapTest gender = (MapTest) object;
			if (gender.ornamentType == this.ornamentType && gender.materialType == this.materialType) {
				result = true;
			}
		}
		return result;
	}

	public int hashCode() {
		int hash = 1;
	//	hash = hash + this.ornamentType.hashCode();
		//hash = hash + this.materialType.hashCode();
		 hash = hash + MapTest.key;
		return hash;
	}

	public void display() {
		for (Map.Entry<Integer, MapTest> entry : maleMap.entrySet()) {
			int key = entry.getKey();
			MapTest gender = entry.getValue();
			System.out.println(key + "     " + gender.ornamentType + " " + gender.materialType + "   "
					+ gender.materialColor + "    " + gender.weight);
			System.out.println("Hashcode   " + entry.hashCode());
		}
	}

}
