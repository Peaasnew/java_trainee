package com.peaas.jewellary.gender;

import com.peaas.jewellary.ornament.*;

import java.util.ArrayList;
import java.util.Iterator;

import com.peaas.jewellary.material.*;

public class Female extends Gender {

	private ArrayList<Gender> femaleOrnament = new ArrayList<Gender>();

	public Female(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor, int weight) {
		super(GenderEnum.FEMALE, ornamentType, materialType, materialColor, weight);

	}

	public void add() {
		newOrnaments = new Gender(GenderEnum.FEMALE, ornamentType, materialType, materialColor, weight);

		femaleOrnament.add(newOrnaments);
	}

	public void display()
	{
		Iterator<Gender> show = femaleOrnament.iterator();
		while(show.hasNext())
		{
			Gender gender = (Gender)show.next();
	 		System.out.println(gender.gender+"  "+gender.ornamentType+" "+gender.materialType+"    "+gender.materialColor+"    "+gender.weight);
		}
	}

}
