package com.peaas.jewellary.gender;

import com.peaas.jewellary.ornament.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

import com.peaas.jewellary.material.*;

public class Male extends Gender {

	private ArrayList<Gender> maleOrnament = new ArrayList<Gender>();
	private Map<Integer, Gender> maleMap = new HashMap<Integer, Gender>();
	private static int key;
	private Gender gender;

	public Male(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor, int weight) {
		super(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);

	}
	
/*	public boolean equals(Object object)
	{
		boolean result = false ;
		Map<Integer, Gender> map;
		map = (Map<Integer, Gender>)object ;
		if(object == null || map.  )
			
		return result;
	} */

	public void add() {
		newOrnaments = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);
		Gender test = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight+5);

		Gender test2 = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight-5);

		maleOrnament.add(newOrnaments);
		if(key!=-1)
		{
		maleMap.put(key++, newOrnaments);
		maleMap.put(key++, test);
		maleMap.put(key++, test2);
		}
		
	}
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null) {
			result = false;
		} else {
			Gender gender = (Gender) object;
			if (gender.ornamentType == this.ornamentType && gender.materialType == this.materialType) {
				result = true;
			}
		}

		return result;
	}

	public int hashCode()
	{
		int hash = 19 ;
		hash = hash + this.ornamentType.hashCode();
		hash = hash + this.materialType.hashCode();
	//	hash = hash + Male.key;
		return hash ;
	}


	public void display()
	{
		Iterator<Gender> show = maleOrnament.iterator();
		while(show.hasNext())
		{
			gender = (Gender)show.next();
		}
		for(Map.Entry<Integer, Gender> entry:maleMap.entrySet())
		{
			int key = entry.getKey();
			Gender gender = entry.getValue();
			System.out.println(key+"  "+gender.gender+"     "+gender.ornamentType+" "+gender.materialType+"   "+gender.materialColor+"    "+gender.weight);
			System.out.println("Key Valur"+ key + "Hashcode"+ entry.hashCode());
		}
	}

}
