package com.peaas.jewellary.gender;

import com.peaas.jewellary.material.*;
import com.peaas.jewellary.ornament.*;

import java.util.*;

public class ArrayListTest extends Gender {

	private ArrayList<Gender> maleOrnament = new ArrayList<Gender>();
	private Gender gender;

	public ArrayListTest(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor, int weight) {
		super(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);

	}

	/*
	 * public boolean equals(Object object) { boolean result = false ;
	 * Map<Integer, Gender> map; map = (Map<Integer, Gender>)object ; if(object
	 * == null || map. )
	 * 
	 * return result; }
	 */

	public void add() {
		newOrnaments = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);
		Gender test = new Gender(GenderEnum.MALE, OrnamentType.RING, materialType, materialColor, weight+5);

		Gender test2 = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight-5);

		maleOrnament.add(newOrnaments);
		maleOrnament.add(test);
		maleOrnament.add(test2);		}

	public boolean equals(Object object) {
		boolean result = false;
		if (object == null) {
			result = false;
		} else {
			Gender gender = (Gender) object;
			if (gender.ornamentType == this.ornamentType && gender.materialType == this.materialType) {
				result = true;
			}
		}

		return result;
	}

	public int hashCode() {
		int hash = 19;
		hash = hash + this.ornamentType.hashCode();
		hash = hash + this.materialType.hashCode();
		// hash = hash + Male.key;
		return hash;
	}

	public void display() {
		Collections.sort(maleOrnament, new Gender(null, ornamentType, materialType, materialColor, weight));
		Iterator<Gender> show = maleOrnament.iterator();
		while (show.hasNext()) {
			gender = (Gender) show.next();
			System.out.println( gender.gender + "     " + gender.ornamentType + " " + gender.materialType
					+ "   " + gender.materialColor + "    " + gender.weight);
		}
	}

}
