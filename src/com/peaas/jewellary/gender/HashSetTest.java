package com.peaas.jewellary.gender;

import com.peaas.jewellary.ornament.*;
import com.peaas.jewellary.material.*;

import java.util.*;

public class HashSetTest extends Gender {

	private HashSet<Gender> maleHashSet = new HashSet<Gender>();
	private Gender gender;

	public HashSetTest(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor, int weight) {
		super(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);

	}

	public void add() {
		newOrnaments = new Gender(GenderEnum.MALE, ornamentType, materialType, materialColor, weight);

		maleHashSet.add(newOrnaments);
	}

	public void display() {

		for(Gender gender:maleHashSet)
		{
			System.out.println(gender.gender+"     "+gender.ornamentType+" "+gender.materialType+"   "+gender.materialColor+"    "+gender.weight);
			
		}
	}

}
