package com.peaas.jewellary.material.main;

import com.peaas.jewellary.material.*;
import com.peaas.jewellary.gender.*;
import com.peaas.jewellary.ornament.*;

import java.io.*;

public class Main {

	static MaterialType materialType = MaterialType.SILVER;
	static OrnamentType ornamentType = OrnamentType.NECKLESS;
	static MaterialColor materialColor = MaterialColor.RED;
	static GenderEnum gender = GenderEnum.MALE;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 0;

		Gender gener;
		//gener = new Male(ornamentType, materialType, materialColor, 10);

		gener = new ArrayListTest(ornamentType, materialType, materialColor, 10);
		// gener = new HashSetTest(ornamentType, materialType,materialColor,
		// 10);

		// gener = new Female(ornamentType, materialType, materialColor, 10);

		try {
			while (true) {
				System.out.println("Press the key : \n 1.Add the ornament 2.Display the ornaments 3.exit");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				i = Integer.parseInt(br.readLine());
				if (i == 1) {
					gener.add();

				} else if (i == 2) {
					System.out.println("gender  Type     Material  Color  weight");
					gener.display();
				} else if (i == 3) {
					System.exit(0);
				} else {
					System.out.println("You pressed the wrong key, please press the right key");
				}

			}
		} catch (Exception ex) {
			System.out.println("Exception" + ex.toString());

		}
	}
}
