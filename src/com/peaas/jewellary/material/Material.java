package com.peaas.jewellary.material;

public class Material {
	public MaterialType materialType;
	public MaterialColor materialColor;
    public int price, cost;

	public Material(MaterialType materialType, MaterialColor materialColor, int price) {
		this.materialType = materialType;
		this.materialColor = materialColor;
		this.price = price;
		this.cost = this.price + materialColor.getExtraCharge();
		}

	public void setCost() {
		this.cost = this.price + materialColor.getExtraCharge();
	}

}
