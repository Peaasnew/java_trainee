package com.peaas.jewellary.ornament;

import com.peaas.jewellary.material.*;

public class Ornament extends Material {

	public OrnamentType ornamentType;

	public Ornament(OrnamentType ornamentType, MaterialType materialType, MaterialColor materialColor) {
		super(materialType, materialColor, 400);
		this.ornamentType = ornamentType;
		this.setCost();
	}

	public void setCost() {
		this.cost = this.cost + ornamentType.get();
	}

}