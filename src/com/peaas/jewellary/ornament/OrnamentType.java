package com.peaas.jewellary.ornament;

public enum OrnamentType { 
	RING(100), NECKLESS(175), EARING(70);
	
	int makingCost ;  //cost is the per gram of oranaments
	   OrnamentType(int makingCost) {
	      this.makingCost = makingCost;
	   }
	   // retutn the cost of the material 
	   public int get() {
	      return makingCost;
	   } 
	}