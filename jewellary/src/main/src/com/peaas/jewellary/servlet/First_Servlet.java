package com.peaas.jewellary.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class First_Servlet
 */
@WebServlet("/First_Servlet")
public class First_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public First_Servlet() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		Connection conn;
		PreparedStatement pstmt;
		Statement stmt;
		ResultSet rs;

		PrintWriter pw = response.getWriter();

		String type = request.getParameter("type");
		int id = Integer.parseInt(request.getParameter("ID"));

		pw.println("<HTML><BODY>");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jewellary", "root", "root");
			String insertQuery = "Insert into material(id,type) values(?,?)";
			pstmt = conn.prepareStatement(insertQuery);
			pstmt.setInt(1, id);
			pstmt.setString(2, type);
			pstmt.executeUpdate();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM material");
			while (rs.next()) {
				pw.print(rs.getInt(1) + "  ");
				pw.print(rs.getString(2) + " <br />");
			}
		} catch (Exception ex) {
			pw.println(ex);
		}

		pw.println("</body></html>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
